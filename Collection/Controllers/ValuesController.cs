﻿using TaskManager.Data_Layer_Access;
using TaskManager.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace TaskManager.Controllers
{
    public class ValuesController : ApiController
    {
        // GET: api/Values
        public List<CustomTask> Get()
        {
            using (TasksContext db = new TasksContext())
            {
                var tasks = db.tasks.ToList();
                return tasks;
            }
        }

        // GET: api/Values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Values
        public void Post(CustomTask tasks)
        {
            using(TasksContext db = new TasksContext())
            {
                db.tasks.Add(tasks);
                db.SaveChanges();
            }
        }

        // PUT: api/Values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Values/5
        public void Delete(int id)
        {
        }
    }
}

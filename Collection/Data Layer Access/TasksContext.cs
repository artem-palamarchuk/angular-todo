namespace TaskManager.Data_Layer_Access
{
    using Models;
    using System.Data.Entity;

    public class TasksContext : DbContext
    {
        public TasksContext()
            : base("name=TaskManagerContext")
        { }

        public DbSet<CustomTask> tasks { get; set; }

    }

}
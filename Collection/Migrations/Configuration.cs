namespace TaskManager.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Data_Layer_Access.TasksContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "TaskManager.Data_Layer_Access.CollectionContext";
        }

        protected override void Seed(Data_Layer_Access.TasksContext context)
        {

        }
    }
}

﻿namespace TaskManager.Models
{
    public abstract class BaseForID
    {
        public abstract int ID { get; set; }
    }
}
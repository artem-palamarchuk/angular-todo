﻿namespace TaskManager.Models
{
    public class CustomTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Expired { get; set; }
        public int Label { get; set; }
    }
}

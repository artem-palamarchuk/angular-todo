﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.SignalR;
using System.Timers;
using Newtonsoft.Json;
using TaskManager.Models;
using TaskManager.Data_Layer_Access;
using System.Text.RegularExpressions;

namespace Web.Hubs
{
    public class TaskHub : Hub
    {

        static List<CustomTask> tasks = new List<CustomTask>();

        static Timer timer = new Timer(5000);
        TasksContext db = new TasksContext();

        bool isTimerRun = false;
                   

        public void Send(string task)
        {
            var converted = JsonConvert.DeserializeObject<CustomTask>(task);
            Clients.All.addMessage(task);
            db.tasks.Add(converted);
            db.SaveChanges();
            tasks = db.tasks.ToList();
            db.Dispose();
        }

        public void DeleteTask(string Label)
        {
            CustomTask task = db.tasks.Where(b => b.Label.ToString() == Label).First();
            db.tasks.Remove(task);
            Clients.All.deleteTask(task);
            db.SaveChanges();
        }

        public void StartTimer()
        {
            tasks = db.tasks.ToList();
            if (!isTimerRun)
            {
                timer.Elapsed += OnElapsedHandler;
                timer.Start();
            }

            isTimerRun = true;
        }

        private void OnElapsedHandler(object sender, ElapsedEventArgs e)
        {

            int time = Int32.Parse(Regex.Replace(DateTime.Now.ToLongTimeString(), ":", ""));

            for (int i = 0; i < tasks.Count; i++)
            {
                if (tasks[i].Expired <= time)
                {
                    Clients.All.pushNotification(tasks[i].Label);
                }
            }

        }
    }
}
﻿$(function () {

    var list = $.connection.taskHub;

    $.connection.hub.logging = true;
    //Function for invoke hub after we get the message(task)
    list.client.addMessage = function (message) {
        // Add message(task) on the page
        var newMessage = JSON.parse(message);
        $('.content .row').append(
            '<div _ngcontent-c1 data-id="'+ newMessage.Label+'" id="task" class="col-xs-8 col-sm-6 col-md-5 col-md-offset-3 col-sm-offset-2 col-xs-offset-1">' +
                '<h3 _ngcontent-c1>' +
                   ' <i _ngcontent-c1 class="fa fa-circle" aria-hidden="true"></i>' +
                    '<span _ngcontent-c1>' + newMessage.Name + '</span>' +
               '</h3>'+
               '<div _ngcontent-c1 class="col-xs-11 col-md-3">' +
                    '<div _ngcontent-c1 class="task-done" title="' + newMessage.Label + '">' +
                        '<i _ngcontent-c1 class="fa fa-check-circle-o" aria-hidden="true"></i> Done!' +
                    '</div>'+
                '</div>'+
                '<div _ngcontent-c1 class="col-xs-11 col-md-4 col-md-offset-2">' +
                    '<div _ngcontent-c1 class="task-time">' +
                        '<i _ngcontent-c1 class="fa fa-clock-o" aria-hidden="true"></i> Today: <span _ngcontent-c1>' + newMessage.Expired + '</span>' +
                    '</div>'+
                '</div>'+
                '<div _ngcontent-c1 class="col-xs-11 col-md-3">' +
                    '<div _ngcontent-c1 class="task-remove" title="' + newMessage.Label + '">' +
                        '<i _ngcontent-c1 class="fa fa-trash-o" aria-hidden="true"></i> Remove' +
                    '</div>'+
                '</div>' +
              '</div>'
            );
    };

    /*We save notifications that have already been displayed in order not to display them any more*/
    if (localStorage.getItem('showedNotifications')) {
        var showedNotifications = JSON.parse(localStorage.getItem('showedNotifications'));
    } else {
        var showedNotifications = [];
        localStorage.setItem('showedNotifications', JSON.stringify(showedNotifications));
    }
   
    /*pushNotification from hub*/
    list.client.pushNotification = function (taskId) {
        if (showedNotifications.indexOf(taskId) == -1) {

            showedNotifications.push(taskId);
            localStorage.setItem('showedNotifications', JSON.stringify(showedNotifications));

            var bodyNotification = $('#task[data-id="'+taskId+'"] h3 span').text();

            sendNotification('Time is almost gone!', {
                body: ''+bodyNotification,
                icon: 'https://s-media-cache-ak0.pinimg.com/originals/3f/82/40/3f8240fa1d16d0de6d4e7510b43b37ba.gif',
                dir: 'auto'
            });
        }
    }

    // Open WebSocket connection
    $.connection.hub.start().done(function () {

        /*Push Notification*/
        if (!("Notification" in window)) {
            alert('Your browser does not support HTML Notifications, it needs to be updated.');
        }
        else {
            Notification.requestPermission(function (permission) {
                // If we get permission
                if (permission !== "granted") {
                    alert('You have disabled showing notifications'); // User was decline
                    Notification.requestPermission();
                }
            });
        }

        list.server.startTimer();

        $(document).on('click', '#submitBtn', function () {
            var taskObject = {};
            var inputTime = convertTime($('#timeTask').val()+':00');

                taskObject["Label"] = genId();
                taskObject["Expired"] = inputTime;
                taskObject["Name"] = $('#nameTask').val();
                
                taskObject = JSON.stringify(taskObject);
                console.log(taskObject);
            // Invoke method on hub Send
                list.server.send(taskObject);
        });

        $(document).on('click', '.task-done', function () {
            var currentTaskId = $('.task-done').attr('title')
            $('.row #task[data-id="'+currentTaskId+'"]').empty();
            list.server.deleteTask(this.title);
        });

        $(document).on('click', '.task-remove', function () {
            var currentTaskId = $('.task-done').attr('title')
            $('.row #task[data-id="' + currentTaskId + '"]').empty();
            list.server.deleteTask(this.title);
        });

    });

    list.client.deleteTask = function (item) {
        this.style.display = 'none';
        console.log("Success delete", item);
    }

    function sendNotification(title, options) {
        if (Notification.permission === "granted") {
            var notification = new Notification(title, options);
        }
        else {
            alert('You have disabled notifications! Please accept it.')
            /*Once again we ask*/
            Notification.requestPermission();
            sendNotification(title, options);
        }
    };

    function genId() {
        var rand = 0 - 0.5 + Math.random(6) * (9 - 0 + 1)
        rand = rand.toString().slice(3, 9);
        return rand;
    }

    var inputTime = $('#timeTask').val()+':00';

    function convertTime(time) {
        var milisecondsForTask = 0,
         timeForPushNotification = 0,
         currentDateTime = new Date().getTime();

        time = time.split(':'); 
        milisecondsForTask = ((time[0] * 60 * 60 * 1000) + (time[1] * 60 * 1000)) - 30000;

        milisecondsForTask += currentDateTime; /*getting time without 30sec*/

        timeForPushNotification = new Date(milisecondsForTask); /*getting from miliseconds normal time*/
        timeForPushNotification = timeForPushNotification.getHours()+ ':' + timeForPushNotification.getMinutes() + ':' + timeForPushNotification.getSeconds(); /*only hours minutes and seconds*/
        timeForPushNotification = timeForPushNotification.split(':');

        timeForPushNotification = timeForPushNotification.map(function(item,i){
            if(item.length<2){
                item = '0'+item;
                return item;
            }
            else{
                return item;
            }
        });

        timeForPushNotification = timeForPushNotification.join('');

        return (timeForPushNotification);
    }
});



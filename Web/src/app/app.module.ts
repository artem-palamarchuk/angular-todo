import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpService, SettingsService } from './index';

import { RootComponent, ListComponent, ToolbarComponent, NewComponent } from './index';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        RootComponent,
        ListComponent,
        ToolbarComponent,
        NewComponent
    ],
    providers: [
        HttpService,
        SettingsService        
    ],
    bootstrap: [RootComponent]
})
export class AppModule { }
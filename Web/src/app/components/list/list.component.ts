﻿import { Component } from '@angular/core';

/* Services */
import { HttpService, SettingsService } from '../../index';

/* Classes */
import { CustomTask } from '../../index';

@Component({
    selector: 'list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent {

    constructor(
        private httpService: HttpService,
        private settingsService: SettingsService
    ) { }

    taskList: Array<CustomTask>;

    ngOnInit(): void {
        this.httpService.get('api/values').subscribe(res => {
            this.taskList = res.json();
        })
    }

    openForm() {
        this.settingsService.isFormOpen = true;
    }

}
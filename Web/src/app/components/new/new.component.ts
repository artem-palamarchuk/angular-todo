﻿import { Component } from '@angular/core';

import { FormGroup, FormControl, Validators } from '@angular/forms';

/* Services */
import { HttpService, SettingsService } from '../../index';

import { CustomTask } from '../../index';

@Component({
    selector: 'new',
    templateUrl: './new.component.html',
    styleUrls: ['./new.component.css']
})
export class NewComponent {


    private taskName: FormControl;
    private taskDuration: FormControl;

    private taskForm: FormGroup;

    constructor(
        private httpService: HttpService,
        private settingsService: SettingsService
    ) { }

    formValid: boolean;

    ngOnInit() {
        this.taskName = new FormControl('', [Validators.required, Validators.minLength(4)]);
        this.taskDuration = new FormControl('', [Validators.required]);

        this.taskForm = new FormGroup({
            taskName: this.taskName,
            taskDuration: this.taskDuration
        });

        this.taskForm.valueChanges.subscribe(data => {
            console.log(this.taskForm.controls.taskDuration.valid);
            this.formValid = this.taskForm.valid ? data : false;
        })
    }

    onSubmit(): void {
        if (this.taskForm.invalid) return;
        this.settingsService.isFormOpen = false;
    }

    formErrors = {
        nameError: 'Name should be more than 4 symbols',
        timeError: 'It should be time'
    }

    convertTime(time: any) {
        let milisecondsForTask = 0,
            currentDateMiliseconds = new Date().getTime(),
            timeForPushNotification: Date;

        time = time.split(':');
        milisecondsForTask = ((time[0] * 60 * 60 * 1000) + (time[1] * 60 * 1000)) - 30000;
        milisecondsForTask += currentDateMiliseconds;
        timeForPushNotification = new Date(milisecondsForTask);
        return (timeForPushNotification.getHours() * 60 * 60 * 1000) + (timeForPushNotification.getMinutes() * 60 * 1000) + (timeForPushNotification.getSeconds() * 1000); /*only hours minutes and seconds*/;
    }
}

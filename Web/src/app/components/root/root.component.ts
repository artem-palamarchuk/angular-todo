﻿import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';


import { HttpService } from '../../index';

@Component({
    selector: 'root',
    templateUrl: './root.component.html'
})
export class RootComponent {

    constructor(
        private httpService: HttpService
    ) { }

    tasksLength: number;

    setLength(length: number) {
        this.tasksLength = length;
    }
}

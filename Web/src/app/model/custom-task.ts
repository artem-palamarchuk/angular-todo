﻿export class CustomTask {

    public Name: string;
    public Expired: number;

    constructor(Name: string, Expired: number) {
        this.Name = Name;
        this.Expired = Expired;
    }
}
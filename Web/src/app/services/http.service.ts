﻿import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';

import { SettingsService } from './index';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService {

    constructor(
        private http: Http
    ) {
        this.setHeaders();
    }

    private headers: Headers = new Headers();
    private options = new RequestOptions({ headers: this.headers });

    public get(url: string): Observable<any> {
        return this.http.get(SettingsService.host + url).map((response: Response) => {
            return response;
        }).catch(this.handleError);
    }

    public post(url: string, data: any): Observable<any> {
        return this.http.post(SettingsService.host + url, data, this.options).map((response: Response) => {
            return response;
        }).catch(this.handleError);
    }

    public delete(url: string, data?: string) {
        return this.http.delete(url, data).map(data => {
            return data;
        });
    }


    private setHeaders() {
        this.headers.append("Content-Type", "application/json");
        this.headers.append("Cache-Control", "no-cache");
        this.headers.append("Pragma", "no-cache");
    }

    handleError = (err: any) => err;

}